UPDATE m_live_quality_setting SET member_swing_bone_live = 0 WHERE set_id = 1 AND quality_mode >= 10 AND quality_mode <= 20;
UPDATE m_live_quality_setting SET member_swing_bone_mv = 0 WHERE set_id = 1 AND quality_mode >= 10 AND quality_mode <= 30;
UPDATE m_live_quality_setting SET member_swing_bone_live = 0 WHERE set_id = 2 AND quality_mode >= 10 AND quality_mode <= 20;
UPDATE m_live_quality_setting SET member_swing_bone_mv = 0 WHERE set_id = 2 AND quality_mode >= 10 AND quality_mode <= 30;
UPDATE m_live_quality_setting SET member_swing_bone_live = 0 WHERE set_id = 3 AND quality_mode >= 10 AND quality_mode <= 20;
UPDATE m_live_quality_setting SET member_swing_bone_mv = 0 WHERE set_id = 3 AND quality_mode >= 10 AND quality_mode <= 30;