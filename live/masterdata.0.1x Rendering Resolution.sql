UPDATE m_live_quality_setting SET rendering_resolution = 144 WHERE quality_mode = 10;
UPDATE m_live_quality_setting SET rendering_resolution = 144 WHERE quality_mode = 20;
UPDATE m_live_quality_setting SET rendering_resolution = 96 WHERE quality_mode = 30;
UPDATE m_live_quality_setting SET rendering_resolution = 86 WHERE quality_mode = 40;